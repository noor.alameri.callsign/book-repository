CREATE DATABASE bookrepository;
USE bookrepository;

CREATE TABLE genre(
                      id INT NOT NULL PRIMARY KEY,
                      genre_type CHAR(200)
);

CREATE TABLE author(
                       id INT NOT NULL PRIMARY KEY,
                       first_name CHAR(200),
                       last_name CHAR(200)
);
CREATE TABLE publisher(
                          id INT NOT NULL PRIMARY KEY,
                          name CHAR(200),
                          address VARCHAR(200)
);

CREATE TABLE book(
                     id INT NOT NULL PRIMARY KEY,
                     title VARCHAR(200),
                     genre_id INT,
                     author_id INT,
                     publisher_id INT,
                     dateOfPublication DATE,
                     rating INT,
                     FOREIGN KEY (genre_id) REFERENCES genre(id),
                     FOREIGN KEY (author_id) REFERENCES author(id),
                     FOREIGN KEY (publisher_id) REFERENCES publisher(id)


);