package com.callsign.bookrepository;


import com.callsign.bookrepository.book.Book;
import com.callsign.bookrepository.book.BookRepository;
import com.callsign.bookrepository.book.BookRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;





@SpringBootApplication
@RestController
public class BookRepositoryApplication {

//	@Autowired
//	BookRepository bookRepository;


//	//Add new book to the collection
//	@PostMapping("/book")
//	public Book addBook(@Valid @RequestBody BookRequest bookRequest) {
//		Book book = mappingFromRequest(bookRequest);
//		return bookRepository.save(book);
//	}


	public static void main(String[] args) {
		SpringApplication.run(BookRepositoryApplication.class, args);
	}

//	Book mappingFromRequest(BookRequest bookRequest) {
//		return new Book(bookRequest.title, bookRequest.author, bookRequest.genre, bookRequest.publisher, bookRequest.date_of_publication, bookRequest.rating);
//	}
}

	//	Get all list of books
//	@GetMapping("/book")
//	public List<Book> getBooks() {
//		return bookRepository.findAll();
//	};

	// Get list of books by the title
//	@GetMapping("/book/title")
//	public List<Book> getBooksByTitle(@RequestParam String title) {
//		return bookRepository.findByTitle(title);
//	}
//
//	// Get list of books by the author
//	@GetMapping("/book/author")
//	public List<Book> getBooksByAuthor(@RequestParam String author) {
//		return bookRepository.findByAuthor(author);
//	}
//
//	// Get list of books by the genre
//	@GetMapping("/book/genre")
//	public List<Book> getBooksByGenre(@RequestParam String genre) {
//		return bookRepository.findByGenre(genre);
//	}

	// Get list of books by the publisher
//	@GetMapping("/book/publisher")
//	public List<Book> getBooksByPublisher(@RequestParam String publisher) {
//		return bookRepository.findByPublisher(publisher);
//	}
//
//	//Get list of books by the above and below rating
//	@GetMapping("/book/byRating")
//	public List<Book> getBooksByRating(@RequestParam(value = "rating") double rating, @RequestParam(value = "comparison") String comparison) {
//		if ("above".equalsIgnoreCase(comparison)) {
//			return bookRepository.findByRatingGreaterThan(rating);
//		} else if ("below".equalsIgnoreCase(comparison)) {
//			return bookRepository.findByRatingLessThan(rating);
//		} else {
//			//Handle invalid comparison value
//			throw new IllegalArgumentException("Invalid comparison value. Please use 'above' or 'below'.");
//		}
//	}
//}











