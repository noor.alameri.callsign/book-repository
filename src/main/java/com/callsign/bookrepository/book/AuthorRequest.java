package com.callsign.bookrepository.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthorRequest {
    public int id;
    public String firstName;
    public String lastName;
}
