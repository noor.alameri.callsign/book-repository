package com.callsign.bookrepository.book;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PublisherResponse {
    private int id;
    @NotBlank(message = "Please provide the name of the publisher" )
    public String publisherName;
    @NotBlank(message = "Please provide the address of the publisher" )
    public String publisherAddress;


    }
