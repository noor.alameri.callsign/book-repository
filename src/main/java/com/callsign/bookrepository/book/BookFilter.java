package com.callsign.bookrepository.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookFilter {
        public String title;
        public String authorFirstName;
        public String authorLastName;
        public String genreType;
        public String publisherName;
        public String publisherAddress;
        public String dateOfPublication;
        public Double rating;}

//        public AuthorRequest author;
//        public GenreRequest genre;
//        public PublisherRequest publisher;




