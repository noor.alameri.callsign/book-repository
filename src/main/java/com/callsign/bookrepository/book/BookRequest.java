package com.callsign.bookrepository.book;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookRequest {
    public int id;
    @NotBlank(message = "Please provide the title of the book")
    @JsonProperty("title")
    public String title;

//    @NotBlank(message = "Please provide the author of the book")
//    @JsonProperty("author")
    public AuthorRequest author;

//    @NotBlank(message = "Please provide the genre of the book")
//    @JsonProperty("genre")
    public GenreRequest genre;

//    @NotBlank(message = "Please provide the publisher of the book")
//    @JsonProperty("publisher")
    public PublisherRequest publisher;

    @NotBlank(message = "Please provide the date of publication of the book")
    @JsonProperty("dateOfPublication")
    public String dateOfPublication;

    @JsonProperty("rating")
    @Range(min = 1, max = 5, message = "Please rate the book between 1 to 5")
    public Double rating;}








