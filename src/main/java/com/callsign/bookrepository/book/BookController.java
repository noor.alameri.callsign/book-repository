package com.callsign.bookrepository.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@Data
@NoArgsConstructor
@AllArgsConstructor
@RestController
@RequestMapping(path = "/book")
public class BookController {
    @Autowired
    private BookRepository bookRepository;

    ;

    //	Get all list of books
//    @GetMapping
//    public List<BookResponse> getBooks() {
//        return bookRepository.findAll().stream().map(this::mappingToResponse).collect(Collectors.toList());
//    }

   @GetMapping
   public List<BookResponse> getBooksByFilter(@RequestParam (required = false) String title,
                                              @RequestParam (required = false) String firstName,
                                              @RequestParam (required = false) String lastName,
                                              @RequestParam (required = false) String genreType,
                                              @RequestParam (required = false) String publisherName,
                                              @RequestParam (required = false) String publisherAddress,
                                              @RequestParam (required = false) String dateOfPublication,
                                              @RequestParam(required = false) Double aboveRating,
                                              @RequestParam (required = false) Double belowRating) {
        //	Get all list of books by title
       if (title != null) {
           return bookRepository.findBooksByTitle(title).stream().map(this::mappingToResponse).collect(Collectors.toList());
       }

        //	Get all list of books by author first name
       if (firstName != null) {
           return bookRepository.findBooksByAuthorFirstNameContains(firstName).stream().map(this::mappingToResponse).collect(Collectors.toList());
       }

        //	Get all list of books by author last name
       if (lastName != null) {
           return bookRepository.findBooksByAuthorLastNameContains(lastName).stream().map(this::mappingToResponse).collect(Collectors.toList());
       }

       //	Get all list of books by their genre
       if (genreType != null) {
           return bookRepository.findBooksByGenreContains(genreType).stream().map(this::mappingToResponse).collect(Collectors.toList());
       }

       //	Get all list of books by the publisher name
       if (publisherName != null) {
           return bookRepository.findBookByPublisherNameContains(publisherName).stream().map(this::mappingToResponse).collect(Collectors.toList());
       }

       //	Get all list of books by the publisher address
       if (publisherAddress != null) {
           return bookRepository.findBookByPublisherAddressContains(publisherAddress).stream().map(this::mappingToResponse).collect(Collectors.toList());
       }

       //	Get all list of books by the date of publication
       if (dateOfPublication != null) {
           return bookRepository.findBooksByDateOfPublication(dateOfPublication).stream().map(this::mappingToResponse).collect(Collectors.toList());
       }

       //	Get all list of books above certain rating
       if (aboveRating != null) {
           return bookRepository.findBooksByRatingGreaterThan(aboveRating).stream().map(this::mappingToResponse).collect(Collectors.toList());
       }

       //	Get all list of books below certain rating
       if (belowRating != null) {
           return bookRepository.findBooksByRatingLessThan(belowRating).stream().map(this::mappingToResponse).collect(Collectors.toList());
       }


    //	Get all list of books
       return bookRepository.findAll().stream().map(this::mappingToResponse).collect(Collectors.toList());}




//    @GetMapping
//    public List<BookResponse> getBooksByFilter(@RequestParam String ){
//
//    }

    //Add new book to the collection
    @PostMapping
    public ResponseEntity<BookResponse> addBook(@RequestBody @Valid BookRequest bookRequest) {
        Book book = mappingFromRequest(bookRequest);
        Book savedBook = bookRepository.save(book);
        BookResponse bookResponse = mappingToResponse(savedBook);
        return ResponseEntity.status(HttpStatus.CREATED).body(bookResponse);
    }


    Book mappingFromRequest(BookRequest bookRequest) {

        Author author = Author.builder()
                .firstName(bookRequest.author.firstName)
                .lastName(bookRequest.author.lastName)
                .build();
        Genre genre = Genre.builder()
                .genre_type(bookRequest.genre.genreType)
                .build();
        Publisher publisher = Publisher.builder()
                .name(bookRequest.publisher.publisherName)
                .address(bookRequest.publisher.publisherAddress)
                .build();

        return Book.builder().title(bookRequest.title).
                author(author).genre(genre).publisher(publisher).
                dateOfPublication(bookRequest.dateOfPublication).rating(bookRequest.rating).build();
    }

    BookResponse mappingToResponse(Book book) {

        AuthorResponse author = AuthorResponse.builder()
                .id(book.getId())
                .firstName(book.getAuthor().getFirstName())
                .lastName(book.getAuthor().getLastName())
                .build();
        GenreResponse genre = GenreResponse.builder()
                .id(book.getId())
                .genreType(book.getGenre().getGenre_type())
                .build();
        PublisherResponse publisher = PublisherResponse.builder()
                .id(book.getId())
                .publisherName(book.getPublisher().getName())
                .publisherAddress(book.getPublisher().getAddress())
                .build();

        return BookResponse.builder().id(book.getId()).title(book.getTitle()).
                author(author).genre(genre).publisher(publisher).
                dateOfPublication(book.getDateOfPublication()).rating(book.getRating()).build();
    }


}
