package com.callsign.bookrepository.book;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookResponse {
    public int id;
    public String title;

    public AuthorResponse author;


    public GenreResponse genre;

    public PublisherResponse publisher;


    public String dateOfPublication;


    public Double rating;}
