package com.callsign.bookrepository.book;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.List;
@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {

    List <Book> findBooksByTitle(String title);
    List <Book> findBooksByAuthorFirstNameContains(String firstName);
    List <Book> findBooksByAuthorLastNameContains(String lastName);
    List <Book> findBooksByGenreContains(String genreType);
    
    List <Book> findBookByPublisherNameContains(String publisherName);
    List <Book> findBooksByDateOfPublication(String dateOfPublication);

    List <Book> findBookByPublisherAddressContains(String publisherAddress);

    List <Book> findBooksByRatingGreaterThan(Double aboveRating);
    List <Book> findBooksByRatingLessThan(Double belowRating);



}