package com.callsign.bookrepository.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PublisherRequest {
    public int id;
    public String publisherName;
    public String publisherAddress;
}
